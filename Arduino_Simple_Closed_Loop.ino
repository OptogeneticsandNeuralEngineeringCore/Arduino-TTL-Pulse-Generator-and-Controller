/*
   Simple Arduino 4 Channel TTL Closed Loop Generator
   Optogenetics and Neural Engineering Core ONE Core
   University of Colorado
   10/5/16
   See ohttps://optogeneticsandneuralengineeringcore.gitlab.io/ONECoreSite/ for more information, including a detailed write up and Arduino/BNC housing 3D Model

   In this example, a TTL input is received by the Arduino, and then a signal is sent out on a separate channel.
   We wish to set up an experiment wherein an animal is trained to poke it's nose in a port. We wish to sense when this occurs and then deliver an optogenetic stimulation, a train of pulses.
   We can detect the nose poke with an IR Beam sensor. A LED emits IR light and a sensor senses the light. When the IR beam is broken, we can sense it with the Arduino.
   The Sensor will connect to TTL1 and the laser will be controlled on TTL2.
*/

//This section defines the pins on the Arduino. We specify the intigers (int) TTL1, TTL2, and TTL3.
//If you expanding this to require more than two channels, uncomment (remove "//") below.
int TTL1 = 2;       //The input from the IR sensor comes in here
int TTL2 = 4;       //The optogenetic output will come from here
int TTL3 = 7;       //The IR sensors will get powered from here
//int TTL4 = 8;     //Not used in this example
//int TTL5 = 12;    //Can be set as a TTL
//int TTL6 = 13;    //Can be set as a TTL, though, not recommend, as there are slight differences in this pin

//This section defines variables that the Arduino will use to define the state of the sensor
int sensorState = LOW, i = 0;

//Now we write the pulse train for the optogenetic stimulation.
//Draw out the desired waves for the stimulation. The condition (or state) of the Arduino changes any time any of the channels change. See the write up for more information.
//The number of conditions is dependent on how many times the Arduino changes. For the given example, the laser is to be pulsed on for 25 ms, off for 25, on for 25, then off for 75. This pulse train is to be delivered twice for a given nose poke.
//Define the number of conditions. Define the length of time (in ms) for each condition.
//Define the number of times to repeat the pulse train
int con1 = 25;
int con2 = 25;
int con3 = 25;
int con4 = 75;
int numrepeat = 2;

//This section sets up the Arduino to treat the pins as output or input. With this example, Channel 1 (TTL1) is input (from an IR Beam) and Channel 2 is a TTL ouput (to a laser).
void setup() 
{
  pinMode(TTL1, INPUT);
  pinMode(TTL2, OUTPUT);
  pinMode(TTL3, OUTPUT);
  digitalWrite(TTL1, HIGH);   //This defines the original state of the sensor (when it is open and detecting IR light) as high/on
  digitalWrite(TTL2,LOW);     //The initial state of the optogenetic stim should be off
  digitalWrite(TTL3, HIGH);   //This turns on TTL3. We do not change it in the rest of the code so that we can use this to power the IR light and sensor
}

//This is the code that the Arduino loops through. It starts with the sensor sensing IR light and waits until the light beam is broken. It then delivers the stimulation through the set of conditions.
//Output will start on condition 1 and then move through the rest of the conditions. It writes each pin as High/ON or Low/Off for each condition, and the waits for the length of time for each condition.
void loop() 
{
  //Read the IR sensor
  sensorState = digitalRead (TTL1);

  //Check to see if the beam is broken.
  if (sensorState == LOW) 
  {
    //If it is indeed broken (LOW), then we output to the laser as such
    //A 'for' command is used to loop through all conditions of the pulse twice.
    for (int i = 0; i < numrepeat; i++) 
    {
      //Condition 1             //Condition code. This is the pulse train of the Optogenetic Stimulation
      digitalWrite(TTL2, HIGH);
      delay(con1);

      //Condition 2
      digitalWrite(TTL2, LOW);
      delay(con2);

      //Condition 3
      digitalWrite(TTL2, HIGH);
      delay(con3);

      //Condition 4
      digitalWrite(TTL2, LOW);
      delay(con4);

      //This states that when the optogenetic stimulation loop is completed, then set the read pin back to high, just as was done in the initialization.
      //You need not worry too much about this, but if you would like more information, SparkFun has a pretty good tutorial on 'floating' and 'pull up resistors'
      if (i == numrepeat - 1) 
      {
      digitalWrite(TTL1, HIGH);
      }
    }
  }
  
//  else //If the sensor state is NOT low (the IR beam is detected), we could add additional code here
//  {
//   //Code to execute if the IR Bean is not broken
//  }


  //Without this section, optogenetic stimulation would be provided if the beam was continuously broken. We want only one stimulation pattern for a given nose poke.
  //Reread the IR sensor. We want to check that the mouse has left the nose poke before we deliver another stimulation.
  while (sensorState == LOW) 
  {
    delay(500);
    sensorState = digitalRead (TTL1);
  }
  

}

